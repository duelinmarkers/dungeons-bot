from src import util

def test_globals() -> None:

    assert bool(util.logger)
    assert len(util.root) > 0
    assert len(util.emojis) > 0
    assert len(util.config) > 0


def test_format_duration() -> None:

    # Minutes
    s = util.format_duration(0)
    assert s == "00:00"
    s = util.format_duration(1)
    assert s == "00:00"

    s = util.format_duration(60)
    assert s == "00:01"
    s = util.format_duration(119)
    assert s == "00:01"

    # Hours
    s = util.format_duration(3600)
    assert s == "01:00"
    s = util.format_duration(7199)
    assert s == "01:59"

    # Days
    s = util.format_duration(86400)
    assert s == "1 day, 00:00"
    s = util.format_duration(93599)
    assert s == "1 day, 01:59"
    s = util.format_duration(172800)
    assert s == "2 days, 00:00"
