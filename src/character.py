import dnd
import random
import math
import logging
import util
from creature import Creature
import lang
import client

class Character(Creature):
    """
    Describes a player Character.
    """

    def __init__(self, name: str) -> None:
        """Creates a new D&D character with a random race, class, and stats."""

        super().__init__(name.title())
        logging.info(f"Creating character '{name}'")

        # Random race
        races = dnd.get_races()
        self._race = dnd.get_race(random.choice(races)["index"])
        logging.info(f"Set race to {self._race['name']}")

        # Random class
        classes = dnd.get_classes()
        self._class = dnd.get_class(random.choice(classes)["index"])
        logging.info(f"Set class to {self._class['name']}")

        # Level
        self._level = 1
        self._hit_dice = 1
        self._xp = 0

        self._proficiency_bonus = dnd.get_class_level(self._class["index"], self._level)["prof_bonus"]

        self._stats = {
            "kills": 0
        }


    def init(self) -> None:

        util.info("Initializing character")

        # Roll for ability scores
        # TODO: this priority will likely need to change when spells can be used
        rolls = []
        for _ in range(len(self._abilities)):
            rolls.append(dnd.roll_max("4d6", t = 3))

        rolls.sort()
        ability_priority = []
        # prefer saving throws
        for saving_throw in self._class["saving_throws"]:
            ability_priority.append(saving_throw["index"])

        # then con and dex and str
        if ("con" not in ability_priority): ability_priority.append("con")
        if ("dex" not in ability_priority): ability_priority.append("dex")
        if ("str" not in ability_priority): ability_priority.append("str")

        # then abilities with racial bonus
        for bonus in self._race["ability_bonuses"]:
            if (bonus["ability_score"]["index"] not in ability_priority):
                ability_priority.append(bonus["ability_score"]["index"])

        for ability in ability_priority:
            self._abilities[ability] = rolls.pop()

        # assign the rest randomly
        for ability in self._abilities:
            if (self._abilities[ability] == 0):
                self._abilities[ability] = random.choice(rolls)
                rolls.remove(self._abilities[ability])

        # Apply racial ability bonuses
        for bonus in self._race["ability_bonuses"]:
            ability = bonus["ability_score"]["index"]
            self._abilities[ability] += bonus["bonus"]

        # Apply ability constraints
        for ability in self._abilities:
            self._abilities[ability] = min(20, self._abilities[ability])

        # Class proficiencies
        for p in self._class["proficiencies"]:
            self.add_proficiency(p["index"])

        # Race proficiencies
        for p in self._race["starting_proficiencies"]:
            self.add_proficiency(p["index"])

        # Choice (class) proficiencies
        prof_choice = self._class["proficiency_choices"][0]
        amount = prof_choice["choose"]
        choices = prof_choice["from"]["options"]
        for _ in range(amount):
            choice = random.choice(choices)
            choices.remove(choice)
            self.add_proficiency(choice["item"]["index"])

        weapon_options = []
        armor_options = []

        def add_equipment_option(e):
            equipment = dnd.get_equipment(e)
            if (equipment["equipment_category"]["index"] == "weapon" and "damage" in equipment):
                weapon_options.append(equipment)
            elif (equipment["equipment_category"]["index"] == "armor" and equipment["armor_category"].lower() != "shield"):
                armor_options.append(equipment)
            else:
                logging.debug(f"{e} not a valid weapon or armor")

        # Add all base starting weapons
        for e in self._class["starting_equipment"]:
            add_equipment_option(e["equipment"]["index"])

        # TODO: make this recursive or something ick
        # this is disgusting im so sorry
        for option in self._class["starting_equipment_options"]:

            if (option["from"]["option_set_type"] == "equipment_category"):
                equipment = dnd.get_equipment_category(option["from"]["equipment_category"]["index"])["equipment"]
                for e in equipment:
                    add_equipment_option(e["index"])

            elif (option["from"]["option_set_type"] == "options_array"):
                for o in option["from"]["options"]:
                    if (o["option_type"] == "choice"):
                        equipment = dnd.get_equipment_category(o["choice"]["from"]["equipment_category"]["index"])["equipment"]
                        for e in equipment:
                            add_equipment_option(e["index"])

                    elif (o["option_type"] == "multiple"):
                        for t in o["items"]:
                            if (t["option_type"] == "counted_reference"):
                                add_equipment_option(t["of"]["index"])

                            elif (t["option_type"] == "choice"):
                                equipment = dnd.get_equipment_category(t["choice"]["from"]["equipment_category"]["index"])["equipment"]
                                for e in equipment:
                                    add_equipment_option(e["index"])

                    elif (o["option_type"] == "counted_reference"):
                        add_equipment_option(o["of"]["index"])

        self._weapon = random.choice(weapon_options) if weapon_options else {}
        self._armor = random.choice(armor_options) if armor_options else {}
        self._gp = dnd.roll_sum("4d4") * 10

        self.set_max_hitpoints(self._class["hit_die"] + self.ability_modifier("con"))
        self._hitpoints = self._max_hitpoints

        util.info(f"Created Character {self}")


    def proficient_weapons(self) -> list[dict]:
        """Get a list of all weapons usable by the Character."""

        weapons = []
        for proficiency in self._proficiencies:
            if (proficiency["type"].lower() == "weapons"):
                category = dnd.get_equipment_category(proficiency["reference"]["index"])
                if (category):
                    for w in category["equipment"]:
                        weapon = dnd.get_equipment(w["index"])
                        if (weapon and "damage" in weapon):
                            weapons.append(weapon)
                else:
                    weapon = dnd.get_equipment(proficiency["reference"]["index"])
                    if (weapon and "damage" in weapon):
                        weapons.append(weapon)

        return weapons


    def proficient_armor(self) -> list[dict]:
        """Get a list of all armor usable by the Character."""

        armors = []
        for proficiency in self._proficiencies:
            if (proficiency["type"].lower() == "armor"):
                category = dnd.get_equipment_category(proficiency["reference"]["index"])
                if (category):
                    for a in category["equipment"]:
                        armor = dnd.get_equipment(a["index"])
                        if (armor and armor["armor_category"].lower() != "shield"):
                            armors.append(armor)
                else:
                    armor = dnd.get_equipment(proficiency["reference"]["index"])
                    if (armor and armor["armor_category"].lower() != "shield"):
                        armors.append(armor)

        return armors


    def armor_class(self, armor: dict = None) -> int:

        if (armor is None): armor = self._armor

        ac = 10 + self.ability_modifier("dex")

        if (armor):
            ac = armor["armor_class"]["base"]
            if (armor["armor_class"]["dex_bonus"]):
                if ("max_bonus" in armor["armor_class"]):
                    ac += min(armor["armor_class"]["max_bonus"], self.ability_modifier("dex"))
                else:
                    ac += self.ability_modifier("dex")

        return ac


    def weapon_modifier(self, weapon: dict = None) -> int:
        """Get the ability modifier for the Character's weapon or the given weapon."""

        if (weapon is None): weapon = self._weapon
        modifiers = dnd.get_weapon_ability(self._weapon)
        modifier = 0

        for m in modifiers:
            modifier = max(modifier, self.ability_modifier(m))

        return modifier


    def spend(self, gold: int) -> int:
        """Spend the given amount of gold."""

        self._gp -= gold
        return self._gp


    def earn(self, gold: int) -> int:
        """Earn the given amount of gold."""

        self._gp += gold
        return self._gp


    def equip(self, equipment: dict) -> None:
        """Equip the given equipment (armor or weapon). Returns True on success."""

        if (equipment["equipment_category"]["index"] == "weapon"):
            self._weapon = equipment
            return True
        elif (equipment["equipment_category"]["index"] == "armor"):
            self._armor = equipment
            return True

        return False


    def attack(self, other: Creature) -> dnd.AttackResult:

        hit_check = dnd.roll_sum("1d20")
        prof_bonus = self._proficiency_bonus
        modifier = self.weapon_modifier()
        hit = 0

        util.debug(f"Character Attack roll: {hit_check} + {prof_bonus} + {modifier}")

        # Critical success
        if (hit_check == 20):
            hit = dnd.roll_sum(self._weapon["damage"]["damage_dice"], self._weapon["damage"]["damage_dice"]) + modifier
            return dnd.AttackResult(max(1, hit), dnd.AttackType.CRIT_SUCCESS, self._weapon["name"])

        # Critical failure
        elif (hit_check == 1):
            return dnd.AttackResult(0, dnd.AttackType.CRIT_FAILURE, self._weapon["name"])

        elif (hit_check + prof_bonus + modifier >= other.armor_class()):
            hit = dnd.roll_sum(self._weapon["damage"]["damage_dice"]) + modifier
            return dnd.AttackResult(max(1, hit), dnd.AttackType.NORMAL, self._weapon["name"])

        return dnd.AttackResult(0, dnd.AttackType.MISS, self._weapon["name"])


    def run(self, other_speed: int) -> bool:
        """Try to run from a creature with the given speed, returning True if successful"""

        return self._race["speed"] >= other_speed


    def death_saving_throws(self) -> bool:
        """
        Perform death saving throws, returning True if throws stabilize the Character.
        If the Character becomes stable, they also heal 1 hitpoint (differs from normal rules).
        """

        successes = 0
        failures = 0
        while (successes < 3 and failures < 3):
            roll = dnd.roll_sum("1d20")
            if (roll == 20):
                successes = 3
            elif (roll == 1):
                failures += 2
            elif (roll >= 10):
                successes += 1
            else:
                failures += 1

        if (successes >= 3):
            self._hitpoints = 1

        return successes >= 3


    def add_experience(self, xp: int) -> None:
        """Add experience to the Character, leveling up if needed."""

        old_level = self._level
        self._xp += xp
        self._level = dnd.experience_level(self._xp)

        # Level up
        if (self._level > old_level):
            self._max_hitpoints += max(1, dnd.roll_sum(f"1d{self._class['hit_die']}") + self.ability_modifier("con"))
            self._proficiency_bonus = dnd.get_class_level(self._class["index"], self._level)["prof_bonus"]
            client.post_status("{} {} reached level {}!".format(util.emojis["experience"]["level"], self._name, self._level))
            return True

        return False


    def short_rest(self) -> int:
        """
        Performs a short rest.
        Rolls available hit dice until depleted or health is back to full.
        Returns hitpoints regained.
        """

        hitpoints_gained = 0
        while (self._hitpoints < self._max_hitpoints and self._hit_dice > 0):
            roll = max(1, dnd.roll_sum(f"1d{self._class['hit_die']}") + self.ability_modifier("con"))
            hitpoints_gained += self.heal(roll)
            self._hit_dice -= 1

        return hitpoints_gained


    def long_rest(self) -> int:
        """
        Performs a long rest.
        Health returns to full and half of maximum hit dice are replenished.
        Returns hitpoints regained.
        """

        old_hitpoints = self._hitpoints
        self._hitpoints = self._max_hitpoints
        self._hit_dice = min(self._level, self._hit_dice + math.ceil(self._level / 2))
        return self._hitpoints - old_hitpoints


    def title(self) -> str:
        """Get the Character's title; including their name, race, and class."""

        return f"{self._name}, the {self._race['name']} {self._class['name']}"


    def emoji(self) -> str:
        """Get the emoji that represents the Character's class."""

        return util.emojis["classes"][self._class["index"]]


    def bio(self) -> str:
        """Create a bio string for this Character."""

        title = "{} {}".format(self.emoji(), self.title())
        level = "{} {} ({})".format(
            util.emojis["experience"]["level"],
            self._level, self._xp
        )
        modifier = self.weapon_modifier()
        weapon = "{} {} [{}, {}{}]".format(
            util.emojis["equipment"]["weapon"],
            self._weapon["name"],
            self._weapon["damage"]["damage_dice"],
            "+" if modifier >= 0 else "-",
            modifier
        )
        armor = "{} {} [{}]".format(
            util.emojis["equipment"]["armor"],
            self._armor["name"] if self._armor else "none",
            self.armor_class()
        )
        gold = "{} {}".format(
            util.emojis["equipment"]["gold"],
            self._gp
        )

        return lang.bio_character.long.choose(
            title,
            self.hitpoint_bar(),
            level,
            weapon,
            armor,
            gold,
            " ".join(f"{a.title()} {self._abilities[a]}" for a in self._abilities)
        )


    def short_bio(self) -> str:
        """Create a death recap string for this Character."""

        health = "{} {}".format(
            util.emojis["health"]["full"],
            self._max_hitpoints
        )
        level = "{} {}".format(
            util.emojis["experience"]["level"],
            self._level
        )
        gold = "{} {}".format(
            util.emojis["equipment"]["gold"],
            self._gp
        )
        kills = "{} {}".format(
            util.emojis["health"]["death"],
            self._stats["kills"]
        )

        return lang.bio_character.short.choose(
            util.emojis["health"]["grave"],
            self.title(),
            health,
            level,
            gold,
            kills
        )


    def __str__(self) -> str:

        return f"""{self._name}
        {self._race['name']}
        {self._class['name']}
        {self._weapon['name']}"""
