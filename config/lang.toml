# Anything added here must be also added in src/lang.py

[adventure]
    [adventure.result]
    default = [
        "You left with a sense of adventure.",
        "You embarked on a journey of adventure.",
    ]

[attack_character]
    # 0: weapon name
    # 1: enemy name
    # 2: damage done
    [attack_character.result]
    default = [
        "You used your {0} to hit the {1} for {2} damage.",
        "You managed to hit the {1} for {2} damage with your {0}.",
        "You successfully hit the {1} with your {0} for {2} damage.",
    ]
    crit_success = [
        "You used your {0} with incredible skill, critically hitting the {1} for {2} damage.",
        "You handled your {0} with expert precision, critically hitting the {1} for {2} damage.",
        "You handled your {0} with grace, dealing a critical blow to the {1} for {2} damage.",
        "You expertly wielded your {0} in the face of the {1}, dealing {2} critical damage.",
        "You utilized your {0} perfectly, dealing {2} critical damage to the {1}.",
    ]
    crit_fail = [
        "You fumbled with your {0}, missing the {1} entirely.",
        "You couldn't even get your {0} out in time to hit the {1} at all.",
    ]
    miss = [
        "You used your {0} to attack the {1} but missed.",
    ]

[attack_monster]
    # 0: monster name
    # 1: attack name
    # 2: damage done
    [attack_monster.result]
    default = [
        "The {0} used {1}, dealing {2} damage to you.",
        "You took {2} damage from the {0}'s {1}.",
        "The {0} attacked with its {1}, you took {2} damage.",
        "You were hit by the {0}'s {1} attack, you took {2} damage.",
    ]
    crit_success = [
        "The {0} attacked with great ferocity with its {1}, dealing {2} critical damage to you.",
        "The {0} used its {1} perfectly, dealing {2} critical damage to you.",
        "THe {0} dealt a critical blow to you with its {1}, you took {2} damage."
    ]
    crit_fail = [
        "The {0} tried to attack with its {1}, but fumbled over itself.",
        "The {0} attacked with {1}, but it missed entirely.",
        "The {0} made a poor attempt to attack with its {1}, you came through unscathed."
    ]
    miss = [
        "The {0} attacked with {1}, but missed.",
        "The {0} attacked with {1}, but you were able to avoid any damage.",
    ]

[avoid]
    # 0: monster name
    [avoid.result]
    default = [
        "You snuck past the {0}.",
        "You managed to avoid the {0}.",
        "You managed to slip past the {0} unnoticed.",
        "You deftly avoided the {0}'s sight.",
    ]

# 0: character long bio
# 1: campaign id
# 2: campaign duration
[bio_campaign]
default = [
    "{0}\n\n{1} {2}",
]

[bio_character]
# 0: character emoji and title
# 1: hitpoints
# 2: level (xp)
# 3: weapon
# 4: armor
# 5: gold
# 6: abilities
long = [
    "{0}\n{1}\n{2}\n{3}\n{4}\n{5}\n{6}",
]
# 0: grave emoji
# 1: character name
# 2: hitpoints
# 3: experience level
# 4: gold
# 5: kills
short = [
    "{0} Here lies {1}\n{2} {3} {4}\n{5}"
]
# 0: hitpoints
# 1: weapon dice
# 2: AC
# 3: level
bar = [
    "{0} {1} {2} {3}"
]

[bio_monster]
# 0: monster emoji and name
# 1: hitpoint bar
# 2: armor
# 3: challenge rating
# 4: xp
long = [
    "{0}\n{1}\n{2}\n{3}\n{4}",
]
# 0: monster emoji and name
# 1: hitpoints
# 2: challenge rating
# 3: xp
short = [
    "{0}\n{1}\n{2}\n{3}",
]
# 0: hitpoints
# 1: challenge rating
# 2: armor class
bar = [
    "{0} {1} {2}"
]

[create]
    [create.prompt]
    default = [
        "This is the legend of...",
        "This is the tale of...",
        "This is the chronicle of...",
        "This is the story of...",
    ]
    # 0: character name
    [create.result]
    default = [
        "{0} was born!",
        "{0} has arrived in town!",
        "Our hero, {0}, has arrived!",
    ]

[death_character]
    [death_character.result]
    default = [
        "You got knocked unconscious and ultimately succumbed to your injuries.",
        "You got knocked down and your injuries were too great.",
        "You fell unconscious and couldn't find the strength to get back up.",
        "You fell unconscious and drifted away.",
    ]
    instant = [
        "You suffered a heavy blow and were instantly killed.",
        "You took heavy damage, you died instantly.",
        "You took damage too great and died instantly.",
        "You suffered heavy damage and were killed.",
    ]
    unconscious = [
        "You got knocked unconscious, but managed to get back up.",
        "You went down for a moment, but managed to get back up.",
        "You got knocked down, but somehow found the strength to get back up.",
        "You briefly lost consciousness, but managed to regain your footing.",
    ]

[death_monster]
    # 0: monster name
    [death_monster.result]
    default = [
        "The {0} died.",
        "The {0} fell, ending the battle.",
        "You killed the {0}.",
        "You won the battle, killing the {0}.",
    ]

[fight]
    # 0: monster emoji and name
    [fight.prompt]
    default = [
        "In the distance you see a {0}",
        "Ahead of your path, you see a {0}",
        "You encounter a {0}",
    ]
    # 0: monster name
    [fight.result]
    default = [
        "You engaged the {0}!",
        "You confronted the {0}!",
    ]

[rest_long]
    # 0: hitpoints regained
    [rest_long.result]
    default = [
        "You slept long through the night, regaining {0} hitpoints.",
        "After a long deserved rest, you woke up feeling refreshed and with {0} more hitpoints.",
        "You rested peacefully through the night, regaining {0} hitpoints.",
        "You slept soundly through the night, regaining {0} hitpoints.",
        "You drifted off to sleep, regaining {0} hitpoints when you woke.",
    ]

[rest_short]
    # 0: hitpoints regained
    [rest_short.result]
    default = [
        "You rested for a moment, regaining {0} hitpoints.",
        "You had a moment to take a breather, you regained {0} hitpoints.",
        "You took a short rest, restoring {0} hitpoints.",
        "You took a few moments to relax, restoring {0} hitpoints.",
        "You sat down and caught your breath, regaining {0} hitpoints.",
    ]

# 0: reward(s)
[reward]
default = [
    "You gained {0}.",
    "You found {0}.",
    "You were rewarded {0}.",
    "You received {0}.",
]

[purchase]
    # 0: purchase equipment name
    [purchase.result]
    default = [
        "You decided to buy the {0} and left the shop.",
        "You found the {0} a great deal and decided to buy it.",
        "You left the shop with the {0} in-hand.",
        "You purchased the {0} and left the shop.",
    ]

[run_character]
    [run_character.result]
    default = [
        "You successfully ran away!",
        "You ran away!",
        "You managed to run away!",
        "You barely managed to get away!",
    ]

[shop]
    [shop.prompt]
    default = [
        "You step into a local shop, what catches your eye?",
        "You browser the ware of a shop, what do you want to purchase?",
        "You enter a shop and see a variety of items, what do you want to buy?",
        "The shop keeper greets you as you enter, what catches your eye?",
        "The shop keeper nods in greeting as you enter, what would you like?",
    ]
    [shop.result]
    default = [
        "You walked to a local shop.",
        "You asked for directions to a nearby shop.",
        "You found a shop.",
    ]

[town]
    [town.prompt]
    default = [
        "You are safe in a small village, what would you like to do?",
        "You are safe in a town, what would you like to do?",
        "You find refuge in a bustling town, what would you like to do?",
        "The walls of a town provide a sense of security, what would you like to do?",
    ]
    [town.result]
    default = [
        "You traveled to a nearby town.",
        "You made the journey to a nearby town.",
        "You found your way to a nearby village.",
    ]
